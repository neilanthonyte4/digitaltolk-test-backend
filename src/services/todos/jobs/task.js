const OneSignal = require('onesignal-node');
const { MongoClient, ObjectId } = require('mongodb');

let db = null;
const url = 'mongodb+srv://admin:asdasd@cluster0.kqf4n.mongodb.net';
const mongodbInstance = new MongoClient(url);

const client = new OneSignal.Client('7bf8e8ad-018c-4708-979c-efc15fddede9', 'MzBiZjUzNWUtNjVmYy00ODVkLTlhYmMtNTQ1ZmZlOWE3MDc2');

module.exports = function () {
    return {
        dbCon: async () => {
            await mongodbInstance.connect();
            console.log('Connected successfully to server');
            db = mongodbInstance.db('digitaltolk').collection('todos');
        },
        processPush: async () => {
            if(db) {
                const allTodos = await db.find({isDeleted: false, isDone: false, device: 'android'}).toArray();
                
                if(allTodos.length < 1) {
                    console.log('no thing to process');
                } else {
                    for(let i = 0; i < allTodos.length; i+=1) {
                        const {deadline, _id, pushToken} = allTodos[i];
                        const end = new Date(deadline).getTime();
                        const now = new Date().getTime();

                        console.log('ONE PENDING TO BE PROCESSED', pushToken);

                        if(now >= end) {
                            const notification = {
                                contents: {
                                    'en': `Your todo deadline is up.`,
                                },
                                include_player_ids: [pushToken]
                            };
    
                            try {
                                const response = await client.createNotification(notification);
                                await db.updateOne({_id: ObjectId(_id.toString())}, {
                                    $set: {isDone: true}
                                }, { upsert: true })

                                console.log('Task Successfully Push',response.body.id);
                              } catch (e) {
                                if (e instanceof OneSignal.HTTPError) {
                                    // When status code of HTTP response is not 2xx, HTTPError is thrown.
                                    console.log(e.statusCode);
                                    console.log(e.body);
                                }
                            }
    
                            client.createNotification(notification)
                            .then(() => {})
                            .catch(e => console.log(e));
                        }
                    }
                }
            }
        }
    }    
}