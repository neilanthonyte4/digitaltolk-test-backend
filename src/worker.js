const Resque = require('node-resque')
const Redis = require('ioredis')

const config = require('./config')
const Jobs = require('./jobs')
const task = require('./services/todos/jobs/task');

start()

async function start () {
    const connection = {
        redis: new Redis(config.redis.url)
    }

    const worker = await createWorker(connection)

    process.on('SIGTERM', shutdown)
    process.on('SIGINT', shutdown)

    async function shutdown () {
        await worker.end()
        console.log('bye.')
        process.exit()
    }
}

async function createWorker (connection) {
    const queues = [
        task().dbCon()
    ]
    const jobs = Jobs(config)
    const worker = new Resque.Worker({ connection, queues }, jobs)
    await worker.connect()
    worker.start()
  
    worker.on('start', () => { console.log('worker started') })
    worker.on('end', () => { console.log('worker ended') })
    worker.on('cleaning_worker', (worker, pid) => { console.log(`cleaning old worker ${worker}`) })
    worker.on('poll', (queue) => {
        task().processPush()
    })
    worker.on('job', (queue, job) => { console.log(`working job ${queue} ${JSON.stringify(job)}`) })
    worker.on('reEnqueue', (queue, job, plugin) => { console.log(`reEnqueue job (${plugin}) ${queue} ${JSON.stringify(job)}`) })
    worker.on('success', (queue, job, result) => { console.log(`job success ${queue} ${JSON.stringify(job)} >> ${result}`) })
    worker.on('failure', (queue, job, failure) => { console.log(`job failure ${queue} ${JSON.stringify(job)} >> ${failure}`) })
    worker.on('error', (error, queue, job) => { console.log(`error ${queue} ${JSON.stringify(job)}  >> ${error}`) })
    worker.on('pause', () => { console.log('worker paused') })
  
    return worker
}